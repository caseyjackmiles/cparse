CParse
======

For EECS 665.
Parses a set of grammar productions.

Running instructions
--------------------

To make the Python file executable, use
	make executable

To run a test of CParse on exampleProductions.txt, use
	make test

To view a diff of exampleProductions with expected output:
	make diff

If you want to use a different input text file:
	./cparse <inputFile.txt>
