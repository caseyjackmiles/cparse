import re, copy

class Production(object):
	""" Stores information about grammar productions """

	def __init__(self, grammarString):
		# Each production has 3 main parts:
		# a LHS, RHS and the placeholder position.
		# The createdState is for when a production
		# has a goto to another state.
		self.LHS = ""
		self.RHS = ""
		self.placeholderPos = 0
		self.createdState = None 

		# Remove newline characters from grammarStrings
		grammarString = grammarString.strip('\n')

		# Split the productions into LHS and RHS
		splitString = re.split("->", grammarString)
		if(len(splitString) == 1):
			# There was no -> in the string.
			# This means that it was the starting production.
			self.LHS = "'"
			self.RHS = splitString[0]
		elif(len(splitString) == 2):
			self.LHS = splitString[0]
			self.RHS = splitString[1]
		else:
			# There should only be one -> in the string.
		 	errorMessage = "Production '%s' has too many '->'" % grammarString
		 	raise Exception(errorMessage)
	
	#==================================
	# Function returns the character
	# that can be 'goto'-ed from this
	# production.
	#==================================
	def charFollowingPlaceholder(self):
		try:
			x = self.RHS[self.placeholderPos]
		except IndexError, e:
			return None
		return x

	#==============================================
	# Given a symbol, check if this production
	# has a goto on that symbol. If so,
	# copy this production and move the placeholder
	#==============================================
	def goto(self, symbol):
		if(self.charFollowingPlaceholder() == symbol):
			#create a copy of the current production	
			prodToReturn = copy.deepcopy(self)
			prodToReturn.placeholderPos += 1
			return prodToReturn
		else:
			return None

	#=========================================
	# String function for printing productions
	#=========================================
	def __str__(self):
		# Turn the RHS into a list and insert
		# the placeholder in the appropriate spot
		printRHS = list(self.RHS)
		printRHS.insert(self.placeholderPos, '@')

		prod = "%s->%s" % (self.LHS, ''.join(printRHS))
		if(self.createdState is not None):
			# Also print createdState
			return "   %-20s %s" % (prod, self.createdState)
		else:
			# Just print the production
			return "   %-20s" % prod

	#=====================================
	# String function to print productions
	# without '@' symbol
	#=====================================
	def stringForGrammar(self):
		return "%s->%s\n" % (self.LHS, self.RHS)

	#==========================================
	# The following functions override equality
	# and allow productions to be places in
	# Python sets.
	#
	# Productions are equal if they have the
	# same LHS, RHS and placeholderPosition.
	#==========================================
	def __key(self):
		return (self.LHS, self.RHS, self.placeholderPos)

	def __eq__(self, other):
		return self.__key() == other.__key()

	def __hash__(self):
		return hash(self.__key())
	

