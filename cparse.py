#!/usr/bin/env python

import sys
from productions import Production
from states import State

def main():
	# Check the command line arguments
	if len(sys.argv) != 2:
		print """Usage:
			./cparse productionsFile.txt """
		sys.exit(0)

	# Read in the original grammar
	# and augment it
	origGrammar = createAugmentedGrammar()

	print "Augmented Grammar\n-----------------"
	for prod in origGrammar:
		sys.stdout.write(prod.stringForGrammar())

	# Get a list of all the grammar symbols
	grammarSymbols = retrieveAllGrammarSymbols()

	# Create a list of states
	# And initialize it with s0,
	# which is created from the
	# closure of the first
	# grammar production.
	states = list()
	s0 = State(origGrammar[0])				# Create state object
	s0.closure(origGrammar, grammarSymbols)	# Perform closure

	# Get the rest of the states
	states = getLR0Items(s0, origGrammar, grammarSymbols)

	# Print out the states
	print ""
	print "Sets of LR(0) Items"
	print "-------------------"
	for i in range(0, len(states)):
		print ""
		print "I%d:" % i
		states[i].printState()


def getLR0Items(initState, grammar, symbols):
	# Create a list of states
	states = []
	states.append(initState)

	stateToWorkWith = 0
	state = states[stateToWorkWith]

	setWasAddedTo = True
	while setWasAddedTo or statesLeftToWorkWith:
		origSetSize = len(states)
		
		try:
			# Check that we're within the
			# range of the list
			state = states[stateToWorkWith]
		except Exception, e:
			print e
			sys.exit(1)

		# Perform closure on the state
		state.closure(grammar, symbols)

		# Check if there's a state that can be
		# 'gone-to'
		for symbol in symbols:
			newState = state.goto(symbol, states)
			# If the state isn't already in our 
			# set of states, add it
			if newState is not None and newState not in states:
				states.append(newState)

		
		if(len(states) == origSetSize):
			setWasAddedTo = False
		else:
			setWasAddedTo = True

		if(stateToWorkWith == len(states)-1):
			statesLeftToWorkWith = False
		else:
			statesLeftToWorkWith = True
			# Increment what state we're dealing with
			stateToWorkWith += 1


	return states


def createAugmentedGrammar():
	grammar = list()
	with open(sys.argv[1], 'r') as grammarFile:
		for line in grammarFile.readlines():
			# Create a new Production instance for each line
			try:
				newProd = Production(line)
			except Exception, e:
				print "Error: %s" % e
				sys.exit(0)
			grammar.append(newProd)
	grammarFile.close()

	return grammar

def retrieveAllGrammarSymbols():
	grammarSymbols = list()
	with open(sys.argv[1], 'r') as grammarFile:
		for line in grammarFile.readlines():
			line = line.strip('\n')
			# Get the position of the first '->'
			i = line.find("->")			
			if(i == -1):
				continue
			else:
				symbolsFromLine = list(line[:i]) + list(line[i+2:])

				for symbol in symbolsFromLine:
					if symbol not in grammarSymbols:
						grammarSymbols.append(symbol)

	grammarFile.close()

	# Add the ' symbol, since it's technically
	# a grammar symbol. Shouldn't make any
	# difference, however.
	grammarSymbols.append('\'')

	return grammarSymbols

if __name__ == "__main__":
	main()
