all: diff

executable:
	chmod +x cparse.py

test: executable
	./cparse.py exampleProductions.txt

diff: executable
	./cparse.py exampleProductions.txt > testOutput.txt
	diff testOutput.txt exampleOutput.txt || exit 0

clean:
	rm -rf testOutput.txt
