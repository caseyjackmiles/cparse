import collections
from productions import Production

class State(object):
	""" Stores information about states """

	def __init__(self, grammarProd):
		# Each State has a list of productions,
		# and a state that initially created it
		self.startingProd = grammarProd
		self.prodSet = list()
		self.prodSet.append(self.startingProd)

	#===================================
	# States are equal if their starting
	# productions are equal.
	#===================================
	def __eq__(self, other):
		return (self.startingProd == other.startingProd)

	#=====================================
	# Add a production to the state's list
	#=====================================
	def addProduction(self, grammarProd):
		if grammarProd not in self.prodSet:
			self.prodSet.append(grammarProd)

	#=========================================
	# Find closure on the state's productions.
	# Loops through the states productions
	# until no more are added.
	#=========================================
	def closure(self, grammar, symbols):
		# Create an auxiliary list to add to
		secondSet = list()

		setWasAddedTo = True
		while setWasAddedTo:
			# Find origSetSize to check against later
			origSetSize = len(self.prodSet)

			for stateProd in self.prodSet:
				# Retrieve the 'goto' character, if any
				closureChar = stateProd.charFollowingPlaceholder()
				if closureChar is None:
					continue
				# Find any grammar productions whose
				# LHS matches the 'goto' character
				for grammarProd in grammar:
					if grammarProd.LHS == closureChar:
						# Check that the production isn't already in the state
						if(grammarProd not in self.prodSet):
							# We add to the secondSet so that 
							# we aren't adding to a list we're
							# simultaneously iterating over.
							secondSet.append(grammarProd)

			# Add the productions from secondSet
			for i in secondSet:
				self.prodSet.append(i)
			# Clear out secondSet
			del secondSet[:]

			# Check that we actually added something
			if (len(self.prodSet) == origSetSize):
				setWasAddedTo = False


	#=====================================
	# Checks each production in the state
	# to see which other states it goes to
	#=====================================
	def goto(self, symbol, states):
		# Create a State object
		addedStateForSymbol = None
		for prod in self.prodSet:
			# Check if goto(symbol) is valid
			# for this production
			x = prod.goto(symbol)

			if x is None:
				continue
			else:
				if addedStateForSymbol is None:
					addedStateForSymbol = State(x)		# Create the state
					if addedStateForSymbol not in states:
						# This is to correctly number the production's createdState
						prod.createdState = "goto(%s)=I%d" % (symbol, len(states))
					else:
						# This state already exists, so we need to find it
						# And set prod.createdState to refer to THAT state
						index = states.index(addedStateForSymbol)
						prod.createdState = "goto(%s)=I%d" % (symbol, index)
				else:
					# The state already exists from another
					# production, so add x to that state
					addedStateForSymbol.addProduction(x)

		# Return the goto state
		return addedStateForSymbol
			

	#=============================
	# For printing a state.
	# Prints every production in
	# the state's production list.
	#=============================
	def printState(self):
		for prod in self.prodSet:
			print prod
